import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface ApiResponse {
  data: any[]; // Define la estructura de la propiedad 'data'
}

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {
  datos: any = null; 

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.obtenerDatos(); 
  }

  obtenerDatos() {
    
    this.http.get<ApiResponse>('http://127.0.0.1:8000/api/personas')
      .subscribe(
        (response) => {
          console.log( response.data );
          this.datos = response.data; 
        },
        (error) => {
          console.error('Error al obtener datos:', error);
        }
      );
  }
}