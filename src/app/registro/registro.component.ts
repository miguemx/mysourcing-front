import { Component,  ChangeDetectorRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent {
  nombre: string = '';
  primerApellido: string = '';
  segundoApellido: string = '';
  email: string = '';
  telefono: string = '';
  codigoPostal: string = '';
  estado: string = '';

  constructor(private http: HttpClient, private cdr: ChangeDetectorRef) {}

  onSubmit() {
    const datosPersona = {
      nombre: this.nombre,
      primer_apellido: this.primerApellido,
      segundo_apellido: this.segundoApellido,
      email: this.email,
      telefono: this.telefono,
      cp: this.codigoPostal,
      estado: this.estado
    };

    // Realizar la solicitud HTTP a la API para enviar los datos
    this.http.post('http://127.0.0.1:8000/api/personas', datosPersona)
      .subscribe(
        (response) => {
          console.log('Datos enviados con éxito:', response);
          alert("Usuarios registrado con éxito");
          this.resetForm();
        },
        (error) => {
          if ( error.status == 400 ) {
            alert(error.error.join("\n"));
          }
          console.error('Error al enviar datos:', error);
        }
      );
  }

  consultarEstado() {
    if (this.codigoPostal) {
      this.http.get<any>('http://127.0.0.1:8000/api/codigo-postal/' + this.codigoPostal)
        .subscribe(
          (response) => {
            this.estado = response;
            this.cdr.detectChanges();
          },
          (error) => {
            console.error('Error al consultar el estado:', error);
          }
        );
    }
  }

  resetForm() {
    this.nombre = '';
    this.primerApellido = '';
    this.segundoApellido = '';
    this.email = '';
    this.telefono = '';
    this.codigoPostal = '';
    this.estado = '';
  }
}