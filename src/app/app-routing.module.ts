import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListadoComponent } from "./listado/listado.component";
import { RegistroComponent } from './registro/registro.component';

const routes: Routes = [
  { path: 'consulta', component: ListadoComponent },
  { path: 'registro', component: RegistroComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
